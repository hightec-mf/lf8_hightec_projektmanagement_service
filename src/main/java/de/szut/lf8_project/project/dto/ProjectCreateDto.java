package de.szut.lf8_project.project.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
public class ProjectCreateDto {
    private String description;
    private long ownerId;
    private String customerName;
    private String comment;
    private Date startDate;
    private Date dueDate;
    private Date finishedDate;
}
