package de.szut.lf8_project.project.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
public class ProjectSmallDto {
    private long Id;
    private String description;
    private Date startDate;
    private Date endDate;
    private String role;
}
