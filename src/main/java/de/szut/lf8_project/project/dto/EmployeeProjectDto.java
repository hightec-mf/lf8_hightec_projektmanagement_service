package de.szut.lf8_project.project.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class EmployeeProjectDto {
    private long employeeId;
    private List<ProjectSmallDto> currentProjects;
}
