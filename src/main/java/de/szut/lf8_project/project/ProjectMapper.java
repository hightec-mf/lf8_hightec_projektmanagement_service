package de.szut.lf8_project.project;

import de.szut.lf8_project.project.dto.*;
import de.szut.lf8_project.project.entity.ProjectEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectMapper {

    public ProjectGetDto mapToDto(ProjectEntity projectEntity) {
        return new ProjectGetDto(
                projectEntity.getId(),
                projectEntity.getDescription(),
                projectEntity.getCreatorId(),
                projectEntity.getClientName(),
                projectEntity.getComment(),
                projectEntity.getStartDate(),
                projectEntity.getDueDate(),
                projectEntity.getFinishedDate()
        );
    }

    public ProjectEntity mapToEntity(ProjectCreateDto projectDto) {
        return new ProjectEntity(
                0,
                projectDto.getOwnerId(),
                projectDto.getCustomerName(),
                projectDto.getDescription(),
                projectDto.getComment(),
                projectDto.getStartDate(),
                projectDto.getDueDate(),
                projectDto.getFinishedDate()
        );
    }
    public ProjectEntity mapToEntity(ProjectGetDto projectDto) {
        return new ProjectEntity(
                projectDto.getId(),
                projectDto.getOwnerId(),
                projectDto.getCustomerName(),
                projectDto.getDescription(),
                projectDto.getComment(),
                projectDto.getStartDate(),
                projectDto.getDueDate(),
                projectDto.getFinishedDate()
        );
    }

    public EmployeeProjectDto mapToEmployeeProject(long employeeId, List<ProjectSmallDto> projects) {
        return new EmployeeProjectDto(
                employeeId,
                projects
        );
    }

    public ProjectSmallDto mapToProjectSmall(ProjectEntity projectEntity, String role) {
        return new ProjectSmallDto(
                projectEntity.getId(),
                projectEntity.getDescription(),
                projectEntity.getStartDate(),
                projectEntity.getFinishedDate(),
                role
        );
    }

    public ProjectEmployeeDto mapToProjectEmployee(ProjectEntity project, List<EmployeeDto> employees) {
        return new ProjectEmployeeDto(
                project.getId(),
                project.getDescription(),
                employees
        );
    }
}
