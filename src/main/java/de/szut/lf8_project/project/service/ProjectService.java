package de.szut.lf8_project.project.service;

import de.szut.lf8_project.project.entity.ContributorEntity;
import de.szut.lf8_project.project.entity.ProjectEntity;
import de.szut.lf8_project.project.repository.ContributorRepository;
import de.szut.lf8_project.project.repository.ProjectRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {
    private final ProjectRepository projectRepository;
    private final ContributorRepository contributorRepository;

    public ProjectService(ProjectRepository repository, ContributorRepository contributorRepository) {
        this.projectRepository = repository;
        this.contributorRepository = contributorRepository;
    }

    public ProjectEntity create(ProjectEntity entity) {
        return this.projectRepository.save(entity);
    }

    public ProjectEntity update(ProjectEntity entity) {
        var existingEntity = readById(entity.getId());
        if (existingEntity == null) {
            return null;
        }

        return this.projectRepository.save(entity);
    }

    public List<ProjectEntity> readAll() {
        return this.projectRepository.findAll();
    }

    public ProjectEntity readById(long id) {
        Optional<ProjectEntity> optionalQualification = this.projectRepository.findById(id);
        if (optionalQualification.isEmpty()) {
            return null;
        }
        return optionalQualification.get();
    }

    public void delete(ProjectEntity entity) {
        List<ContributorEntity> contributors = this.contributorRepository.findByProjectId(entity.getId());
        for (ContributorEntity contributor : contributors) {
            this.contributorRepository.delete(contributor);
        }

        this.projectRepository.delete(entity);
    }

    public List<ContributorEntity> readContributorsByProjectId(long projectId) {
        return this.contributorRepository.findByProjectId(projectId);
    }

    public List<ContributorEntity> readContributorsByEmployeeId(long employeeId) {
        return this.contributorRepository.findByEmployeeId(employeeId);
    }

    public ContributorEntity readContributionByProjectIdAndEmployeeId(long projectId, long employeeId) {
        return this.contributorRepository.findByProjectIdAndEmployeeId(projectId, employeeId);
    }

    public void deleteContribution(ContributorEntity entity) {
        this.contributorRepository.delete(entity);
    }

    public ContributorEntity addContribution(ContributorEntity entity) {
        return this.contributorRepository.save(entity);
    }
}
