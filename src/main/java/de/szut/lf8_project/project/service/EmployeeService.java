package de.szut.lf8_project.project.service;

import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EmployeeService {
    private final String serviceUrl = "https://employee.szut.dev";

    private String getBearer() {
        var auth = (SimpleKeycloakAccount)SecurityContextHolder.getContext().getAuthentication().getDetails();
        var token = auth.getKeycloakSecurityContext().getTokenString();

        return "Bearer " + token;
    }

    public boolean employeeExists(long employeeId) {
        var endpoint = serviceUrl + "/employees/" + employeeId;
        var restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", getBearer());

        var entity = new HttpEntity<>(headers);

        try {
            var response = restTemplate.exchange(endpoint, HttpMethod.GET, entity, String.class);
            return response.getStatusCodeValue() == 200;
        }
        catch (Exception e) {
            return false;
        }
    }
}