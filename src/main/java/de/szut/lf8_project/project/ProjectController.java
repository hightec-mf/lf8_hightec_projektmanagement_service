package de.szut.lf8_project.project;

import de.szut.lf8_project.exceptionHandling.ProcessingException;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.project.dto.*;
import de.szut.lf8_project.project.entity.ContributorEntity;
import de.szut.lf8_project.project.entity.ProjectEntity;
import de.szut.lf8_project.project.service.EmployeeService;
import de.szut.lf8_project.project.service.ProjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "projects")
public class ProjectController {
    private final ProjectService projectService;
    private final EmployeeService employeeService;
    private final ProjectMapper mapper;

    public ProjectController(ProjectService projectService, EmployeeService employeeService, ProjectMapper mapper) {
        this.projectService = projectService;
        this.employeeService = employeeService;
        this.mapper = mapper;
    }

    @Operation(summary = "creates a new project with all needed information")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "created new project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "employee not found",
                    content = @Content)
    })
    @PostMapping
    public ProjectGetDto create(@RequestBody @Valid ProjectCreateDto projectCreate) {
        if (!this.employeeService.employeeExists(projectCreate.getOwnerId())) {
            throw new ResourceNotFoundException(String.format("Employee with ID %d doesn't exist", projectCreate.getOwnerId()));
        }

        ProjectEntity projectEntity = this.mapper.mapToEntity(projectCreate);
        projectEntity = this.projectService.create(projectEntity);

        return this.mapper.mapToDto(projectEntity);
    }

    @Operation(summary = "delivers a list of all projects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)
    })
    @GetMapping
    public List<ProjectGetDto> findAll() {
        return this.projectService.readAll()
                .stream()
                .map(this.mapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Operation(summary = "search for a specific project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "404", description = "project not found",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public ProjectGetDto findById(@PathVariable long id) {
        var entity = this.projectService.readById(id);
        if (entity == null) {
            throw new ResourceNotFoundException(String.format("Project with ID %d doesn't exist ", id));
        }

        return this.mapper.mapToDto(entity);
    }

    @Operation(summary = "delete an specific project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "delete successful"),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)})
    @DeleteMapping
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void delete(@RequestParam long id) {
        var entity = this.projectService.readById(id);
        if (entity == null) {
            throw new ResourceNotFoundException(String.format("Project with ID %d doesn't exist ", id));
        }

        this.projectService.delete(entity);
    }

    @Operation(summary = "updates an existing project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)
    })
    @PutMapping
    public ProjectGetDto update(@RequestBody @Valid ProjectGetDto dto) {
        var entity = this.mapper.mapToEntity(dto);
        entity = this.projectService.update(entity);
        if (entity == null) {
            throw new ResourceNotFoundException(String.format("Project with ID %d doesn't exist ", dto.getId()));
        }

        return this.mapper.mapToDto(entity);
    }

    @Operation(summary = "delivers all projects an employee is assigned to")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of all projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = EmployeeProjectDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)
    })
    @GetMapping("employee")
    public EmployeeProjectDto findAllProjectsByEmployeeId(@RequestParam @Valid long employeeId) {
        var contributions = this.projectService.readContributorsByEmployeeId(employeeId);
        if (contributions == null || contributions.isEmpty()) {
            throw new ResourceNotFoundException("No projects found for employee with ID " + employeeId);
        }

        List<ProjectSmallDto> projects = new ArrayList<>();
        for (var contribution : contributions) {
            var project = this.projectService.readById(contribution.getProjectId());
            projects.add(this.mapper.mapToProjectSmall(project, contribution.getRole()));
        }

        return this.mapper.mapToEmployeeProject(employeeId, projects);
    }

    @Operation(summary = "delivers all employees that are assigned to a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of all employees of a project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectEmployeeDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)
    })
    @GetMapping("contributions")
    public ProjectEmployeeDto findAllEmployeesByProjectId(@RequestParam @Valid long projectId) {
        var project = this.projectService.readById(projectId);
        if (project == null) {
            throw new ResourceNotFoundException(String.format("Project with ID %d doesn't exist ", projectId));
        }

        var contributions = this.projectService.readContributorsByProjectId(projectId);
        if (contributions == null) {
            throw new ResourceNotFoundException("Can't fetch employees from project with ID " + projectId);
        }

        List<EmployeeDto> employees = contributions
                .stream()
                .map(entity -> new EmployeeDto(entity.getEmployeeId(), entity.getRole()))
                .collect(Collectors.toList());

        return this.mapper.mapToProjectEmployee(project, employees);
    }

    @Operation(summary = "removes an contributing employee that is assigned to a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "delete successful"),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)
    })
    @DeleteMapping("contributions")
    public void removeEmployeeFromProject(@RequestParam @Valid long projectId, @RequestParam @Valid long employeeId) {
        var contribution = this.projectService.readContributionByProjectIdAndEmployeeId(projectId, employeeId);
        if (contribution == null) {
            throw new ResourceNotFoundException(String.format("Employee with ID %d is not assigned to project with ID %d", projectId, employeeId));
        }

        this.projectService.deleteContribution(contribution);
    }

    @Operation(summary = "Assigns an employee to a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of all employees of a project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectEmployeeDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "failed to add contributor to project",
                    content = @Content)
    })
    @PostMapping("contributions")
    public ProjectEmployeeDto addEmployeeToProject(@RequestParam @Valid long projectId, @RequestParam @Valid long employeeId) {
        var project = this.projectService.readById(projectId);
        if (project == null) {
            throw new ResourceNotFoundException(String.format("Project with ID %d doesn't exist", projectId));
        }

        if (!this.employeeService.employeeExists(employeeId)) {
            throw new ResourceNotFoundException(String.format("Employee with ID %d doesn't exist", employeeId));
        }

        var contribution = this.projectService.readContributionByProjectIdAndEmployeeId(projectId, employeeId);
        if (contribution != null) {
            throw new ResourceNotFoundException(String.format("Employee with ID %d is already assigned to project with ID %d", projectId, employeeId));
        }

        contribution = this.projectService.addContribution(new ContributorEntity(
                0,
                projectId,
                employeeId,
                "contributor"
        ));
        if (contribution == null) {
            throw new ProcessingException(String.format("Failed to add contributing employee '%d' to project '%d'.", employeeId, projectId));
        }

        return findAllEmployeesByProjectId(projectId);
    }
}
