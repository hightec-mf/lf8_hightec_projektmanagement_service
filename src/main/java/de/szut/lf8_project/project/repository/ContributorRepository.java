package de.szut.lf8_project.project.repository;

import de.szut.lf8_project.project.entity.ContributorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContributorRepository extends JpaRepository<ContributorEntity, Long> {
    List<ContributorEntity> findByProjectId(long projectId);
    List<ContributorEntity> findByEmployeeId(long employeeId);
    ContributorEntity findByProjectIdAndEmployeeId(long projectId, long employeeId);
}
